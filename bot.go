package main

import (
"log"
"gopkg.in/telegram-bot-api.v4"
	"os"
)

func main() {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		data := [][]string{
			[]string{"A", update.Message.Text},
			[]string{"B", update.Message.Text},
		}

		bf := CreateTableText(data)
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bf.String())

		bot.Send(msg)
	}
}
