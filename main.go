package main

import (
	"github.com/olekukonko/tablewriter"
	"bytes"
)

func CreateTableText(data [][]string) bytes.Buffer {
	var bf bytes.Buffer
	table := tablewriter.NewWriter(&bf)
	table.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.AppendBulk(data) // Add Bulk Data
	table.Render() // Send output
	return bf
}

//func main() {
//	data := [][]string{
//		[]string{"A", "500"},
//		[]string{"B", "288"},
//		[]string{"C", "120"},
//		[]string{"D", "800"},
//		[]string{"A", "500"},
//		[]string{"B", "288"},
//		[]string{"C", "120"},
//		[]string{"D", "800"},
//	}
//	bf := CreateTableText(data)
//	fmt.Println(bf.String())
//}

